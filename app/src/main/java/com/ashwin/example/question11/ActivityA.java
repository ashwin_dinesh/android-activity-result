package com.ashwin.example.question11;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityA extends AppCompatActivity {

    private static final int REQUEST_CODE = 100;

    private TextView mTextView;
    private Button mGoToButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a);

        initViews();
    }

    private void initViews() {
        mTextView = (TextView) findViewById(R.id.textView);

        mGoToButton = (Button) findViewById(R.id.goToButton);
        mGoToButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityA.this, ActivityB.class);
                startActivityForResult(intent, REQUEST_CODE);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if(resultCode == RESULT_OK) {
                String str = "Name: ";
                str += (!data.getStringExtra("NAME").isEmpty()) ? data.getStringExtra("NAME") : "empty";
                str += "\nCountry: ";
                str += (!data.getStringExtra("COUNTRY").isEmpty()) ? data.getStringExtra("COUNTRY") : "empty";
                mTextView.setText(str);
            } else if (resultCode == RESULT_CANCELED) {
                mTextView.setText("Cancelled by user");
            } else {
                mTextView.setText("Welcome");
            }
        } else {
            mTextView.setText("Welcome");
        }
    }

}
