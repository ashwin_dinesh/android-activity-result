package com.ashwin.example.question11;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ActivityB extends AppCompatActivity {

    private EditText mNameEditText, mCountryEditText;
    private Button mDoneButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b);

        initViews();
    }

    private void initViews() {
        mNameEditText = (EditText) findViewById(R.id.nameEditText);
        mCountryEditText = (EditText) findViewById(R.id.countryEditText);

        mDoneButton = (Button) findViewById(R.id.doneButton);
        mDoneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = mNameEditText.getText().toString();
                String country = mCountryEditText.getText().toString();
                Intent intent = new Intent();
                intent.putExtra("NAME", name);
                intent.putExtra("COUNTRY", country);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }

}
